package bloomfilter

import (
	"hash/crc32"
	"math/rand"
	"strconv"

	"gitlab.com/it.devel.att/bitslice"
)

type BloomFilter struct {
	size          uint64
	hashFunctions []func(v string) uint64
	bits          []bitslice.BitSlice
}

type Filter interface {
	Add(v string)
	NotExists(v string) bool
}

func NewBloomFilter(size uint64, hashFunctions []func(v string) uint64) Filter {
	f := &BloomFilter{size: size, hashFunctions: hashFunctions, bits: make([]bitslice.BitSlice, len(hashFunctions))}
	for i := range hashFunctions {
		f.bits[i] = bitslice.NewBitSlice(size)
	}
	return f
}

func (f *BloomFilter) Add(v string) {
	for i := range f.hashFunctions {
		f.bits[i].Set(f.position(v, i))
	}
}

func (f *BloomFilter) NotExists(v string) bool {
	result := map[bool]int{}
	for i := range f.hashFunctions {
		if f.bits[i].At(f.position(v, i)) {
			result[true]++
			continue
		}
		result[false]++
	}
	notExists := result[false] > 0
	return notExists
}

func (f *BloomFilter) position(v string, i int) uint64 {
	return f.hash(v, i) % f.size
}

func (f *BloomFilter) hash(v string, i int) uint64 {
	return f.hashFunctions[i](v)
}

func hash(postfix string) func(v string) uint64 {
	return func(v string) uint64 {
		return uint64(crc32.Checksum([]byte(v+postfix), crc32.IEEETable))
	}
}

func DeafultHashGenerator() func() func(v string) uint64 {
	initial, step := 100, 100

	return func() func(v string) uint64 {
		postfix := strconv.Itoa(rand.Intn(initial) + step)
		initial += step

		return hash(postfix)
	}

}
