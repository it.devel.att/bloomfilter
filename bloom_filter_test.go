package bloomfilter

import (
	"math/rand"
	"strconv"
	"testing"
	"time"
)

func Test_Add_AndExist(t *testing.T) {
	type testCase struct {
		inputStrings   []string
		checkingString string
		shouldExists   bool
	}

	for _, tc := range []testCase{
		{
			inputStrings:   []string{"hello_world", "xyz"},
			checkingString: "hello_world",
			shouldExists:   true,
		},
		{
			inputStrings:   []string{"hello_world", "xyz"},
			checkingString: "123",
			shouldExists:   false,
		},
		// This is not collision of hashing, but take the same bit position because of numberFromHash % sizeOfSlize
		// false positive example
		{
			inputStrings:   []string{"hello_world", "xyz"},
			checkingString: "zyx",
			shouldExists:   true,
		},
	} {
		t.Run("", func(t *testing.T) {
			filter := NewBloomFilter(uint64(len(tc.inputStrings))*2, []func(v string) uint64{
				hash("postfix"),
			})

			for i := range tc.inputStrings {
				filter.Add(tc.inputStrings[i])
			}
			notExists := filter.NotExists(tc.checkingString)

			if tc.shouldExists && notExists {
				t.Errorf("element '%v' should exist", tc.checkingString)
			}

			if !tc.shouldExists && !notExists {
				t.Errorf("element '%v' shouldn't exist", tc.checkingString)
			}
		})
	}
}

func Test_FalsePositiveResultsUnder_4_Percent(t *testing.T) {
	rand.Seed(time.Now().UnixMicro())
	upperBoundPercent := float32(4.0)
	elements := 1_000_000
	generate := DeafultHashGenerator()

	filter := NewBloomFilter(uint64(elements), []func(v string) uint64{
		generate(),
		generate(),
		generate(),
	})

	for i := 0; i < elements; i++ {
		if i%2 == 0 {
			word := strconv.Itoa(i)
			filter.Add(word)
		}
	}

	for i := 0; i < elements; i++ {
		if i%2 == 0 && filter.NotExists(strconv.Itoa(i)) {
			t.Errorf("element %v should exist", i)
			t.FailNow()
		}
	}

	falsePositiveCount := 0
	for i := 0; i < elements; i++ {
		if i%2 != 0 && !filter.NotExists(strconv.Itoa(i)) {
			falsePositiveCount++
		}
	}
	falsePositivePercent := percent(falsePositiveCount, elements)
	if falsePositivePercent > upperBoundPercent {
		t.Errorf("false positive result should not exceed %v percent. Actual percent: %v", upperBoundPercent, falsePositiveCount)
	}
}

func percent(one, another int) float32 {
	return float32(one) / float32(another) * 100
}
